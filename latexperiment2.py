#!/usr/bin/python
"""
Taking a crack at making a python file that accepts input arguments and makes them into a latex file
"""

#begin-doc-include
from pylatex import Document, Section, Subsection, Command
from pylatex.utils import italic, NoEscape

def fill_doc_input(doc):
    sectiontext = raw_input("What do you want to say in the main section? ")
    subsectiontext = raw_input("What do you want to say in the first subsection? ")
    with doc.create(Section('The Main Section')):
        doc.append(sectiontext)
        doc.append(italic(sectiontext + ' , the same text but in italic'))

        with doc.create(Subsection('A subsection')):
            doc.append(subsectiontext)

if __name__ == '__main__':
    #Basic document
    doc = Document('basic')
    fill_doc_input(doc)

    doc.generate_pdf()
    doc.generate_tex()

    #Document with `\maketitle` command activated

    doc = Document()

    doc.preamble.append(Command('title', 'Awesome Title'))
    doc.preamble.append(Command('author', 'Awesome author'))
    doc.preamble.append(Command('date', NoEspace(r'\today')))
    doc.append(NoEscape(r'\maketitle'))

    fill_doc_input(doc)

    doc.generate_pdf('basic_maketitle', clean=False)

    #Add stuff to the document
    with doc.create(Section('Another one')):
        doc.append('More text.')

    doc.generate_pdf('basic_maketitle2')
    tex = doc.dumps() #the document as string in LaTeX syntax
