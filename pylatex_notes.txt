so pylatex looks pretty cool, I know the
__name__ == '__main__'
thig is important in coding, but I don't really understand it right now and would
prefer not to use it for the time being, despite its use in the pylatex examples

that being said, it doesn't need to be used and the rest of the library looks
immensely useful for easily creating latex-styled pdfs from a python file

additionally, it looks like the easiest and best pratice for using this would be to
create a TeX template document using either a single or a series of functions [def]
(possibly even made into a comprehensive module [class]) and then call the function
and supply it with the custom input information

before going any deeper into TeX formatting or python scripting though - this still
needs a gui interface - it's probably best to determine what the exact PAIR proposal layout would be and how it would need to live on the PAIR project website. (My guess is I
could do some kind of python webapp, but maybe there is a better language to use
ultimately, in which case I'd have to explore a different scripting option
