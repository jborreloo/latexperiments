#!/usr/bin/python
"""ok so now let me see if I can make a python script that doesn't have the name in main thing going on, which I believe only means I can call the function from the command line without using python before it"""

from pylatex import Document, Section, Subsection, Command
from pylatex.utils import italic, NoEscape

sectiontext = raw_input("What do you want to say in the first section? ")
subsectiontext = raw_input("What do you want to say in the subsection? ")

doc = Document('basicly')

with doc.create(Section('A section')):
    doc.append(sectiontext)

with doc.create(Subsection('A subsection')):
    doc.append(subsectiontext)

doc.generate_pdf()
